var mainScriptEditor = document.getElementById("mainScriptEditor");
mainScript = mainScriptEditor.value;

var canvas = document.getElementById("canvas");
canvas.width = 1024;
canvas.height = 568;

canvas.oncontextmenu = function (e) {
  e.preventDefault();
};

var statusElement = document.getElementById('moduleStatusString');
var downloadingSymbolRightArrow = false;

var Module = {
  setStatus: function(text) {
    try {
      var regexMatch = text.match("([0-9]*)\/([0-9]*)");
      var progress = "";
      if (regexMatch != null && regexMatch.length == 3) {
        var downloaded = parseFloat(regexMatch[1]) / 1024.0 / 1024.0;
        var total = parseFloat(regexMatch[2]) / 1024.0 / 1024.0;
        if (total == 0) {
          total = 1;
        }
        progress = Math.round(((downloaded / total) * 100)).toString() + "%";
        var downloadingSymbol = downloadingSymbolRightArrow == true ? ">" : "<";
        downloadingSymbolRightArrow = !downloadingSymbolRightArrow;
        var outputString = downloadingSymbol + " " + downloaded.toFixed(2).toString() + " MB / " + total.toFixed(2).toString() + " MB " + progress;
        statusElement.innerHTML = outputString;
      }
      else {
        statusElement.innerHTML = ""
      }
    }
    catch (error) {
      console.log(error);
      statusElement.innerHTML = error
    }
  }

};
Module.canvas = canvas;
