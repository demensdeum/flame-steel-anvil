// https://stackoverflow.com/questions/47313403/passing-client-files-to-webassembly-from-the-front-end

function loadFiles() {
  var input = document.getElementById("files");
  loadNextFile(-1, input);
};

function loadNextFile(index, input) {
  index += 1;

  if (index > input.files.length - 1) {
    alert("files loading finished");
    return;
  }

  var file = input.files[index];
  var fileReader = new FileReader();
  fileReader.onload = function() {
      var data = new Uint8Array(fileReader.result);
      try {
        FS.createDataFile('/', file.name, data, true, true, true);
      }
      catch (error) {
        alert("File " + file.name + " loading error: " + error);
      }
      loadNextFile(index, input);
  };
  fileReader.readAsArrayBuffer(file);
};

function cleanAndRestart() {
  console.log("cleanAndRestart");
  var mainScriptEditor = document.getElementById("mainScriptEditor");
  mainScript = mainScriptEditor.value;
  var restartFunction = Module.cwrap('FlameSteelEngine_cleanAndRestartScriptEngine', null, null);
  restartFunction();
};
