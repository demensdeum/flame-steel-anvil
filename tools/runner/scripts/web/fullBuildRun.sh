scripts/buildScripts/webassembly/clean.sh
buildDirectory=build/webassembly
mkdir -p $buildDirectory/resources || true
cp project/resourcesSourcesAndTools/src/graphics/primitives/com.demensdeum.flamesteelengine.logo.texture $buildDirectory/resources
cp project/resourcesSourcesAndTools/src/graphics/primitives/com.demensdeum.flamesteelengine.cube.fsglmodel $buildDirectory/resources
scripts/buildScripts/webassembly/build.sh
rm -rf /var/www/html/FlameSteelEngineProject/
cp -r build/webassembly/FlameSteelEngineProject/ /var/www/html/
rm /var/www/html/FlameSteelEngineProject/index.html
cp tools/runner/resources/* /var/www/html/FlameSteelEngineProject/
mv /var/www/html/FlameSteelEngineProject/FlameSteelEngineProject.js /var/www/html/FlameSteelEngineProject/main.js
chromium http://localhost/FlameSteelEngineProject/
