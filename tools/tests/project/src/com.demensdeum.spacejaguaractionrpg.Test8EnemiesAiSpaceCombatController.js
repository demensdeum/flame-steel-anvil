function Test8EnemiesAiSpaceCombatController() {
  this.initializeIfNeeded = function() {
    if (this.initialized != true) {
      this.initialized = true;
      this.spaceTravelMainController = new SpaceTravelMainController();
    }
  };

  this.step = function() {
    this.initializeIfNeeded();
    this.spaceTravelMainController.step();
    if (this.testStarted != true) {
      this.testStarted = true;
      this.spaceTravelMainController.addSpacePirateWithNameAtPosition("SpacePirate", newVector(0, 0, -5));
    }
  };
};
