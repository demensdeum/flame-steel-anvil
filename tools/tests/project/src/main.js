if (GLOBAL_CONTEXT === undefined) {
  include("com.demensdeum.spacejaguaractionrpg.includes.js");
  include("TestsIncludes.js");
  IncludeDependencies();
  GLOBAL_APP_TITLE = "Space Jaguar Action RPG. Tests";
  GLOBAL_CONTEXT = new Context();
  GLOBAL_CONTEXT.initializeGame = function() {
    this.gameplayData.tinyJSBug = true;
    this.gameplayData.tinyJSBug = undefined;
    var newGameDataInitializer = new NewGameDataInitializer();
    newGameDataInitializer.initialize(this.gameplayData);
    var startController = new TestsPickerMainController();
    this.switchToController(startController);
  };
};

GLOBAL_CONTEXT.step();
