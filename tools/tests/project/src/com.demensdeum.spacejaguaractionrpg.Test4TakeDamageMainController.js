function Test4TakeDamageMainController() {

  this.initializeIfNeeded = function() {
    if (this.initialized != true) {
      this.initialized = true;
      this.spaceTravelMainController = new SpaceTravelMainController();
    }
  };
  this.step = function() {
    this.initializeIfNeeded();
    this.spaceTravelMainController.step();
    if (this.testStarted != true) {
      this.testStarted = true;
      this.spaceTravelMainController.addTurretWithNameAtPosition("Turret", newVector(0, 0, -5));
      this.spaceTravelMainController.addShipWithNameAtPosition("JagShip", newVector(0, 0, 0));
      addText("initialized from Test4TakeDamageMainController");
    }
  };
};
