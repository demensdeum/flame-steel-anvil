function Test2WeaponShootingMainController() {

  this.i = 0;

  this.initializeIfNeeded = function() {
    if (this.initialized != true) {
      this.initialized = true;
      this.spaceTravelMainController = new SpaceTravelMainController();
    }
  };

  this.step = function() {
    this.initializeIfNeeded();
    this.spaceTravelMainController.step();
    if (this.testStarted != true) {
      this.testStarted = true;
        this.spaceTravelMainController.switchToNextWeapon(function(result) {
          addText(result.message);
        });
      addText("initialized from Test2WeaponShootingMainController");
    }

    if (this.i % 20 == 0) {
      this.spaceTravelMainController.shoot();
    }
    this.i += 1;
  };
};
