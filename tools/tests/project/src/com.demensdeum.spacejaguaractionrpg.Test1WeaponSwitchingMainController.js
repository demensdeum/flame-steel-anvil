function Test1WeaponSwitchingMainController() {
  GLOBAL_TEST_RUNNER = this;
  this.state = "idle";

  this.initializeIfNeeded = function() {
    if (this.initialized != true) {
      this.initialized = true;
      this.spaceTravelMainController = new SpaceTravelMainController();
    }
  };

  this.step = function() {
    this.initializeIfNeeded();
    this.spaceTravelMainController.step();
    if (this.state == "idle") {
      this.state = "waitingForCompletion";
      addText("Test 1 next weapon");
      this.spaceTravelMainController.switchToNextWeapon(function(result) {
        addText("Test 1 completion: " + result.message);
        if (result.success == false) {
          GLOBAL_TEST_RUNNER.state = "finished";
        }
        else {
          GLOBAL_TEST_RUNNER.state = "idle";
        }
      });
    }
  };
};
