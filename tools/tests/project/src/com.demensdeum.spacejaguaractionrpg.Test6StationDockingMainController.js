function Test6StationDockingMainController() {

  this.initializeIfNeeded = function() {
    if (this.initialized != true) {
      this.initialized = true;
      this.spaceTravelMainController = new SpaceTravelMainController();
    }
  };

  this.step = function() {
    this.initializeIfNeeded();
    this.spaceTravelMainController.step();
    if (this.testStarted != true) {
      this.testStarted = true;
      this.spaceTravelMainController.addStationWithNameAtPosition("Station", newVector(0, 0, -5));
      this.spaceTravelMainController.addShipWithNameAtPosition("JagShip", newVector(0, 0, 0));
      var jagShip = this.spaceTravelMainController.getObject("JagShip");
      jagShip.ai.step = function() {
        if (this.translationAdded != true) {
          var translationVector = newVector(-0.05, 0, 0);
          var translationPhysicsAction = new PhysicsAction(translationVector);
          this.object.physicsActions.add(translationPhysicsAction);
          this.translationAdded = true;
        }
      };
      addText("initialized from Test6StationDockingMainController");
    }
  };
};
