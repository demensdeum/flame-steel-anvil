function Test3TargetDamageMainController() {

  this.initializeIfNeeded = function() {
    if (this.initialized != true) {
      this.initialized = true;
      this.spaceTravelMainController = new SpaceTravelMainController();
      this.spaceTravelMainController.disablePointerLock();
    }
  };

  this.step = function() {
    this.initializeIfNeeded();
    this.spaceTravelMainController.step();
    if (this.testStarted != true) {
      this.testStarted = true;
      this.spaceTravelMainController.step();
      for (var i = 0; i < 4; i++) {
        this.spaceTravelMainController.switchToNextWeapon(function(result) {
          addText(result.message);
        });
      }
      var position = newVector(0, 0, -5);
      this.spaceTravelMainController.addStationWithNameAtPosition("Target", position);
      this.spaceTravelMainController.shoot();
      addText("initialized from Test3TargetDamageMainController");
    }
  };
};
