function TestsPickerMainController() {
  this.initializeIfNeeded = function() {
    if (this.initialized != true) {
      this.initialized = true;
      addDefaultCamera();
      addText("Pick Test");

      this.buttonToControllerMap = new Dictionary();
      this.buttonToControllerMap.set("1. Weapon Switching", Test1WeaponSwitchingMainController);
      this.buttonToControllerMap.set("2. Weapon Shooting", Test2WeaponShootingMainController);
      this.buttonToControllerMap.set("3. Target Damage", Test3TargetDamageMainController);
      this.buttonToControllerMap.set("4. Take Damage", Test4TakeDamageMainController);
      this.buttonToControllerMap.set("5. Star Docking", Test5StarDockingMainController);
      this.buttonToControllerMap.set("6. Station Docking", Test6StationDockingMainController);
      this.buttonToControllerMap.set("7. Planet Docking", Test7PlanetDockingMainController);
      this.buttonToControllerMap.set("8. Enemies Ai Space Combat", Test8EnemiesAiSpaceCombatController);
      this.buttonToControllerMap.set("9. Friend Ai Space Combat", Test9FriendAiSpaceCombatController);
      this.buttonToControllerMap.set("10. Station Generator", Test10StationGeneratorController);
      this.buttonToControllerMap.set("11. Planet Generator", Test11PlanetGeneratorController);
      this.buttonToControllerMap.set("12. Space Generator", Test12SpaceGeneratorController);
      this.buttonToControllerMap.set("13. Abandoned Station Generator", Test13AbandonedStationGeneratorController);
      this.buttonToControllerMap.set("14. Abandoned Ship Generator", Test14AbandonedShipGeneratorController);
      this.buttonToControllerMap.set("15. Battle Inside Ship", Test15BattleInsideShipController);
      this.buttonToControllerMap.set("16. Galaxy Travel Generator", Test16GalaxyTravelGeneratlorController);

      this.buttonToControllerMap.indexedForEach(function (index, keyValue) {
        addButton(keyValue.key, "testButton" + index);
      });
    }
  };

  this.step = function() {
    this.initializeIfNeeded();
    var self = this;
    this.buttonToControllerMap.indexedForEach(function (index, keyValue) {
      if (isButtonPressed("testButton" + index)) {
        var class = keyValue.value;
        var controller = new class();
        self.delegate.switchToController(controller);
      }
    });
  };

}; 
