if (GLOBAL_Context_Initialized != true) {
  include("com.demensdeum.flamesteelanvil.includes.js");
  IncludeDependencies();

  GLOBAL_APP_TITLE = "Flame Steel Anvil 1.0.0";

  var controller = new MainMenuController();

  GLOBAL_Context = new Context();
  GLOBAL_Context_Initialized = true;

  GLOBAL_Context.switchToController(controller);
}

GLOBAL_Context.step();
