function CubesTestController() {

  this.startCubesCount = 65;

  this.addCube = function() {
    var cube = createObject();
    cube.name = "Cube" + GLOBAL_cubesCount;
    cube.modelPath = "com.demensdeum.flamesteelengine.cube.fsglmodel";
    cube.position.x = -2 + Math.randInt(0, 4);
    cube.position.z = -2 - Math.randInt(0, 2);
    cube.rotation.x = 1;
    addObject(cube);

    GLOBAL_cubesCount += 1;
  };

  this.initializeIfNeeded = function() {
    if (this.initialized != true) {
      this.initialized = true;
      GLOBAL_FreeMoveEnabled = false;

      IncludeDependencies();
      addDefaultCamera();

      GLOBAL_FreeViewManager = new FreeViewManager(getObject("camera"), 0.1);

      if (GLOBAL_FreeMoveEnabled) {
        enablePointerLock();
      }

      GLOBAL_Initialized = true;
      GLOBAL_previousDate = Date.now();
      GLOBAL_framesCount = 0;

      GLOBAL_cubesCount = 0;

      print("Flame Steel Engine Started!");

      for (var i = 0; i < this.startCubesCount; i++) {
        this.addCube();
      }
    }
  };

  this.step = function() {
    this.initializeIfNeeded();
    var now = Date.now();
    var diff = now - GLOBAL_previousDate;
    diff = Integer.parseInt(diff);
    GLOBAL_framesCount += 1;

    if (diff >= 1000) {
      print("FPS: " + GLOBAL_framesCount);

      destroyWindow();
      addText("Cubes: " + GLOBAL_cubesCount);
      addText("FPS: " + GLOBAL_framesCount);

      GLOBAL_previousDate = Date.now();
      GLOBAL_framesCount = 0;
      this.addCube();
    }

    for (var i = 0; i < GLOBAL_cubesCount; i++) {
      var cube = getObject("Cube" + i);
      cube.rotation.x += 0.015;
      cube.rotation.y += 0.015;
      updateObject(cube);
    }

    if (GLOBAL_FreeMoveEnabled) {
      GLOBAL_FreeViewManager.step();
    }
  };
};
