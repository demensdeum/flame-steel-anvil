function MainMenuController() {

  this.initializeIfNeeded = function() {
    if (this.initialized != true) {

      GLOBAL_FreeMoveEnabled = false;

      IncludeDependencies();
      addDefaultCamera();

      var cube = createObject();
      cube.name = "Cube";
      cube.modelPath = "com.demensdeum.flamesteelengine.cube.fsglmodel";
      cube.position.z = -2;
      cube.rotation.x = 1;
      addObject(cube);

      GLOBAL_FreeViewManager = new FreeViewManager(getObject("camera"), 0.1);

      if (GLOBAL_FreeMoveEnabled) {
        enablePointerLock();
      }

      this.initialized = true;

      addText("Flame Steel Anvil 1.0.0");
      addButton("Many cubes", "manyCubes");
      print("Flame Steel Engine Started!");
    }
  };

  this.step = function() {
    this.initializeIfNeeded();

    var cube = getObject("Cube");
    cube.rotation.x += 0.015;
    cube.rotation.y += 0.015;
    updateObject(cube);

    if (GLOBAL_FreeMoveEnabled) {
      GLOBAL_FreeViewManager.step();
    }

    if (isButtonPressed("manyCubes")) {
      removeAllObjects();
      var controller = new CubesTestController();
      GLOBAL_Context.switchToController(controller);
    }
  };
};
