#ifndef SPACEJAGUARACTIONRPGLINUXSPACEJAGUARSCRIPTCONTROLLER_H_
#define SPACEJAGUARACTIONRPGLINUXSPACEJAGUARSCRIPTCONTROLLER_H_

#include <memory>
#include <map>
#include "tiny-js/TinyJS.h"
#include "tiny-js/TinyJS_Functions.h"
#include "tiny-js/TinyJS_MathFunctions.h"
#include <FlameSteelCore/SharedNotNullPointer.h>
#include <FlameSteelEngineGameToolkit/Controllers/ScriptController.h>
#include "EcmaScriptControllerDataSource.h"
#include "EcmaScriptControllerDelegate.h"
#include "EcmaScriptControllerCallContainer.h"
#include <FlameSteelBattleHorn/Music.h>
#include <FlameSteelSpiderToolkit/WebSockets/WebSocketsClient.h>
#include <FlameSteelSpiderToolkit/ClientDelegate.h>

#ifdef __EMSCRIPTEN__
#include <SDL_ttf.h>
#else
#include <SDL2/SDL_ttf.h>
#endif

using namespace std;
using namespace Shortcuts;
using namespace FlameSteelEngine::GameToolkit;
using namespace FlameSteelBattleHorn;

static int tinyjSBindingsToFlameSteelEngineGameToolkit_guiTextID_num = 0;

namespace SpaceJaguarActionRPG {

class EcmaScriptController: public ScriptController, public enable_shared_from_this<EcmaScriptController>, public ClientDelegate {

public:
    void setScript(string script);
    void step();
	void playMusic(string);
    weak_ptr<EcmaScriptControllerDataSource> dataSource;
    weak_ptr<EcmaScriptControllerDelegate> delegate;

	void clearCache();
	void clientDidReceiveData(shared_ptr<Client> client, string data);

	map<string, void *> keyToImageCache;

	TTF_Font *font = nullptr;

	shared_ptr<WebSocketsClient> webSocketsClient;

private:
    NotNull<EcmaScriptControllerCallContainer> callContainer = make<EcmaScriptControllerCallContainer>();
    NotNull<CTinyJS> tinyJS = make<CTinyJS>();

    void initialize();
    bool isInitialized = false;

	NotNull<Music> music; 

};
};


#endif
