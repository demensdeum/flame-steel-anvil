#ifndef SPACEJAGUARACTIONRPGLINUXSPACEJAGUARSCRIPTCONTROLLERDELEGATE_H_
#define SPACEJAGUARACTIONRPGLINUXSPACEJAGUARSCRIPTCONTROLLERDELEGATE_H_

using namespace FlameSteelCore;

namespace SpaceJaguarActionRPG {

class EcmaScriptController;

class EcmaScriptControllerDelegate {

public:
    virtual void spaceJaguarScriptControllerDidRequestPlayAnimationForObjectWithName(shared_ptr<EcmaScriptController> spaceJaguarController, string animationName, string objectName) = 0;
    virtual void spaceJaguarScriptControllerDidRequestAddObjectWithPath(shared_ptr<EcmaScriptController> spaceJaguarController, string name, string  modelPath, float x, float y, float z, float rX, float rY, float rZ, float sX, float sY, float sZ, bool surfaceMaterialEnabled, int surfaceWidth, int surfaceHeight, string layer) = 0;
    virtual void spaceJaguarScriptControllerDidRequestUpdateObjectWithNameAndPositionXYZrXrYrZsXsYsZ(shared_ptr<EcmaScriptController> spaceJaguarController, string name, float x, float y, float z, float rX, float rY, float rZ, float sX, float sY, float sZ) = 0;
    virtual void spaceJaguarScriptControllerDidRequestRemoveObjectWithName(shared_ptr<EcmaScriptController> spaceJaguarController, string name) = 0;
    virtual void spaceJaguarScriptControllerDidRequestRemoveAllObjects(shared_ptr<EcmaScriptController> spaceJaguarController) = 0;
    virtual void spaceJaguarScriptControllerDidRequestSetWindowTitle(shared_ptr<EcmaScriptController> spaceJaguarController, string title) = 0;
    virtual void spaceJaguarScriptControllerDidRequestAddButtonWithId(shared_ptr<EcmaScriptController> spaceJaguarController, string title, string id) = 0;
	virtual void spaceJaguarScriptControllerDidRequestDestroyWindow(shared_ptr<EcmaScriptController> spaceJaguarController) = 0;
	virtual void spaceJaguarScriptControllerDidRequestEnablePointerLock(shared_ptr<EcmaScriptController> spaceJaguarController) = 0;
	virtual void spaceJaguarScriptControllerDidRequestDisablePointerLock(shared_ptr<EcmaScriptController> spaceJaguarController) = 0;
};
};

#endif