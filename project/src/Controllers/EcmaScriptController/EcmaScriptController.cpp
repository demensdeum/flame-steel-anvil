#include "EcmaScriptController.h"
#include <iostream>
#include <FlameSteelEngineGameToolkit/Utils/FSEGTUtils.h>

#if __EMSCRIPTEN__
#include <emscripten.h>
#include <emscripten/val.h>
using namespace emscripten;
#endif

#include <FlameSteelEngineGameToolkit/Data/Components/FSEGTFactory.h>
#include <SDL2/SDL_image.h>
#include <filesystem>
#include <unistd.h>
#include <regex>
#include <sstream>

using namespace std;
using namespace SpaceJaguarActionRPG;

void tinyJSBindingsToFlameSteelEngineGameToolkit_saveVariable(CScriptVar *v, void *context) {
	auto container = (EcmaScriptControllerCallContainer *) context;
	shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
	auto tinyJS = container->tinyJS;
	tinyJS->saveVariable(v->getParameter("name")->getString());
}

void tinyjSBindingsToFlameSteelEngineGameToolkit_IsKeyPressed(CScriptVar *v, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    auto key = v->getParameter("key")->getString();
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto tinyJS = container->tinyJS;
    auto dataSourceLocked = spaceJaguarScriptController->dataSource.lock();
    if (dataSourceLocked == nullptr) {
        throwRuntimeException("tinyjSBindingsToFlameSteelEngineGameToolkit_IsKeyPressed error: can't lock dataSource");
    }
    auto isPressed = dataSourceLocked->spaceJaguarScriptControllerAskingIsKeyPressed(spaceJaguarScriptController, key);
    v->getReturnVar()->setInt(isPressed);
}

void tinyJSBindingsToFlameSteelEngineGameToolkit_PlayMusic(CScriptVar *v, void *context) {
	auto path = v->getParameter("path")->getString();
	auto container = (EcmaScriptControllerCallContainer *) context;
	shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
	spaceJaguarScriptController->playMusic(path);
};

void tinyJSBindingsToFlameSteelEngineGameToolkit_Print(CScriptVar *v, void *) {
    cout << "Tiny-JS print: " << v->getParameter("text")->getString() << endl;
}

void tinyjSBindingsToFlameSteelEngineGameToolkit_GetObject__private(CScriptVar *v, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    auto objectName = v->getParameter("text")->getString();
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto tinyJS = container->tinyJS;
    auto dataSourceLocked = spaceJaguarScriptController->dataSource.lock();
    if (dataSourceLocked == nullptr) {
        throwRuntimeException("tinyjSBindingsToFlameSteelEngineGameToolkit_GetObject error: can't lock dataSource");
    }
    auto object = dataSourceLocked->spaceJaguarScriptControllerDidRequestObjectWithName(spaceJaguarScriptController, objectName);
    //cout << "Tiny-JS get object: " << objectName << endl;
    if (object.get() == nullptr) {
        auto errorString = string("");
        errorString += "EcmaScriptController: there is no object with name: ";
        errorString += objectName;
        throwRuntimeException(errorString);
    }

    auto id = object->getInstanceIdentifier();
    auto position = FSEGTUtils::getObjectPosition(object);
    auto rotation = FSEGTUtils::getObjectRotation(object);
    auto scale = FSEGTUtils::getObjectScale(object);

    auto executeString = string("");
    executeString += "getObject__private__CallResult = newObjectWithIdAndPositionXYZrXrYrZsXsYsZ__private(\"";
    executeString += *id.get();
    executeString += "\",";
    executeString += to_string(position->x);
    executeString += ",";
    executeString += to_string(position->y);
    executeString += ",";
    executeString += to_string(position->z);
    executeString += ",";
    executeString += to_string(rotation->x);
    executeString += ",";
    executeString += to_string(rotation->y);
    executeString += ",";
    executeString += to_string(rotation->z);
    executeString += ",";
    executeString += to_string(scale->x);
    executeString += ",";
    executeString += to_string(scale->y);
    executeString += ",";
    executeString += to_string(scale->z);
    executeString += ");";

    //cout << "Get object execute string: " << executeString << endl;

    tinyJS->execute(executeString);
}

void tinyjSBindingsToFlameSteelEngineGameToolkit_selectFile(CScriptVar *v, void *) {
	system("zenity --file-selection > fileSelectionResult.txt");
	system("perl -p -i -e 's/\\R//g;' fileSelectionResult.txt");
	auto modelPath = stringFromFileAtPath("fileSelectionResult.txt");
	v->getReturnVar()->setString(modelPath);
};

void tinyjSBindingsToFlameSteelEngineGameToolkit_exit(CScriptVar *v, void *) {
    auto exitCode = v->getParameter("exitCode")->getInt();
    exit(exitCode);
}

void tinyJSBindingsToFlameSteelEngineGameToolkit_setWindowTitle(CScriptVar *v, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto tinyJS = container->tinyJS;
    auto text = v->getParameter("text")->getString();
    auto delegateLocked = spaceJaguarScriptController->delegate.lock();
    if (delegateLocked == nullptr) {
        throwRuntimeException("tinyJSBindingsToFlameSteelEngineGameToolkit_SetWindowTitle error: can't lock delegate");
    }
    delegateLocked->spaceJaguarScriptControllerDidRequestSetWindowTitle(spaceJaguarScriptController, text);
}

void tinyjSBindingsToFlameSteelEngineGameToolkit_copyFileIntoCurrentPath(CScriptVar *v, void *) {
	auto filePath = v->getParameter("filePath")->getString();
	cout << filePath << endl;
	try {
		std::filesystem::copy(filePath, ".");
		v->getReturnVar()->setInt(true);
	}
	catch (const exception& e) {
		cout << e.what() << endl;
		v->getReturnVar()->setInt(false);
	}
};

void tinyjSBindingsToFlameSteelEngineGameToolkit_PlayAnimation__private(CScriptVar *v, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto animationName = v->getParameter("animationName")->getString();
    auto objectName = v->getParameter("objectName")->getString();
    auto delegateLocked = spaceJaguarScriptController->delegate.lock();
    if (delegateLocked == nullptr) {
        throwRuntimeException("tinyjSBindingsToFlameSteelEngineGameToolkit_PlayAnimation__private error: can't lock delegate");
    }
    delegateLocked->spaceJaguarScriptControllerDidRequestPlayAnimationForObjectWithName(spaceJaguarScriptController, animationName, objectName);
}

void tinyjSBindingsToFlameSteelEngineGameToolkit_AddObject__private(CScriptVar *v, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    auto name = v->getParameter("name")->getString();
    auto modelPath = v->getParameter("modelPath")->getString();
    auto x = v->getParameter("x")->getDouble();
    auto y = v->getParameter("y")->getDouble();
    auto z = v->getParameter("z")->getDouble();
    auto rX = v->getParameter("rX")->getDouble();
    auto rY = v->getParameter("rY")->getDouble();
    auto rZ = v->getParameter("rZ")->getDouble();
    auto sX = v->getParameter("sX")->getDouble();
    auto sY = v->getParameter("sY")->getDouble();
    auto sZ = v->getParameter("sZ")->getDouble();
	auto surfaceMaterialEnabled = v->getParameter("surfaceMaterialEnabled")->getInt();
	auto surfaceWidth = v->getParameter("surfaceWidth")->getInt();
	auto surfaceHeight = v->getParameter("surfaceHeight")->getInt();
	auto layer = v->getParameter("layer")->getString();

    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto tinyJS = container->tinyJS;
    auto delegateLocked = spaceJaguarScriptController->delegate.lock();
    if (delegateLocked == nullptr) {
        throwRuntimeException("tinyjSBindingsToFlameSteelEngineGameToolkit_AddObject error: can't lock delegate");
    }
    delegateLocked->spaceJaguarScriptControllerDidRequestAddObjectWithPath(
		spaceJaguarScriptController,
		name,
		modelPath,
		x, y, z,
		rX, rY, rZ,
		sX, sY, sZ,
		surfaceMaterialEnabled, surfaceWidth, surfaceHeight,
		layer
);
    tinyJS->execute("addObject__private__CallResult = newObjectWithIdAndPositionXYZrXrYrZsXsYsZ__private(0,0,0,0,0,0,0,0,0,0);");
}

void tinyjSBindingsToFlameSteelEngineGameToolkit_Prompt(CScriptVar *v, void *context) {
	auto container = (EcmaScriptControllerCallContainer *) context;
	auto tinyJS = container->tinyJS;
	auto text = v->getParameter("text")->getString();
	string inputText;
	cout << "Tiny-JS print: "<< text << endl;
#if __EMSCRIPTEN__
	EM_ASM({
		var promptText = UTF8ToString($0);
		__global_flamesteelenginegametoolkit_prompt_result = prompt(promptText);
		}, text.c_str());
	val rawAction = val::global("__global_flamesteelenginegametoolkit_prompt_result");
	inputText = rawAction.as<string>();
#else
	getline (cin, inputText);
#endif
	v->getReturnVar()->setString(inputText);
}

void tinyjSBindingsToFlameSteelEngineGameToolkit_UpdateObject__private(CScriptVar *v, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    auto name = v->getParameter("name")->getString();
    auto x = v->getParameter("x")->getDouble();
    auto y = v->getParameter("y")->getDouble();
    auto z = v->getParameter("z")->getDouble();
    auto rX = v->getParameter("rX")->getDouble();
    auto rY = v->getParameter("rY")->getDouble();
    auto rZ = v->getParameter("rZ")->getDouble();
    auto sX = v->getParameter("sX")->getDouble();
    auto sY = v->getParameter("sY")->getDouble();
    auto sZ = v->getParameter("sZ")->getDouble();
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto tinyJS = container->tinyJS;
    auto delegateLocked = spaceJaguarScriptController->delegate.lock();
    if (delegateLocked == nullptr) {
        throwRuntimeException("tinyjSBindingsToFlameSteelEngineGameToolkit_UpdateObject error: can't lock delegate");
    }

    delegateLocked->spaceJaguarScriptControllerDidRequestUpdateObjectWithNameAndPositionXYZrXrYrZsXsYsZ(spaceJaguarScriptController, name, x, y, z, rX, rY, rZ, sX, sY, sZ);
}

void tinyjSBindingsToFlameSteelEngineGameToolkit_RemoveObject__private(CScriptVar *v, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    auto name = v->getParameter("name")->getString();
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto tinyJS = container->tinyJS;
    auto delegateLocked = spaceJaguarScriptController->delegate.lock();
    if (delegateLocked == nullptr) {
        throwRuntimeException("tinyjSBindingsToFlameSteelEngineGameToolkit_UpdateObject error: can't lock delegate");
    }
    delegateLocked->spaceJaguarScriptControllerDidRequestRemoveObjectWithName(spaceJaguarScriptController, name);
}

void tinyjSBindingsToFlameSteelEngineGameToolkit_RemoveAllObjects__private(CScriptVar *, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto tinyJS = container->tinyJS;
    auto delegateLocked = spaceJaguarScriptController->delegate.lock();
    if (delegateLocked == nullptr) {
        throwRuntimeException("tinyjSBindingsToFlameSteelEngineGameToolkit_UpdateObject error: can't lock delegate");
    }
    delegateLocked->spaceJaguarScriptControllerDidRequestRemoveAllObjects(spaceJaguarScriptController);

	spaceJaguarScriptController->clearCache();
}

void tinyjSBindingsToFlameSteelEngineGameToolkit_IsButtonPressed__private(CScriptVar *v, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto tinyJS = container->tinyJS;
    auto dataSourceLocked = spaceJaguarScriptController->dataSource.lock();
    if (dataSourceLocked == nullptr) {
        throwRuntimeException("tinyjSBindingsToFlameSteelEngineGameToolkit_AddObjectObject error: can't lock delegate");
    }
    auto id = v->getParameter("id")->getString();
	auto result = dataSourceLocked->spaceJaguarScriptControllerAskingIsButtonPressed(spaceJaguarScriptController, id);
	v->getReturnVar()->setInt(result);
};

void tinyjSBindingsToFlameSteelEngineGameToolkit_isPointerPressed__private(CScriptVar *v, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto tinyJS = container->tinyJS;
    auto dataSourceLocked = spaceJaguarScriptController->dataSource.lock();
    if (dataSourceLocked == nullptr) {
        throwRuntimeException("tinyjSBindingsToFlameSteelEngineGameToolkit_AddObjectObject error: can't lock delegate");
    }
	auto result = dataSourceLocked->spaceJaguarScriptControllerAskingIsPointerPressed(spaceJaguarScriptController);
	v->getReturnVar()->setInt(result);
};

void tinyjSBindingsToFlameSteelEngineGameToolkit_pressedPointerParameter__private(CScriptVar *v, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto tinyJS = container->tinyJS;
    auto dataSourceLocked = spaceJaguarScriptController->dataSource.lock();
    if (dataSourceLocked == nullptr) {
        throwRuntimeException("tinyjSBindingsToFlameSteelEngineGameToolkit_AddObjectObject error: can't lock delegate");
    }
    auto id = v->getParameter("parameter")->getString();
	auto result = dataSourceLocked->spaceJaguarScriptControllerAskingForPressedPointerParameter(spaceJaguarScriptController, id);
	v->getReturnVar()->setDouble(result);
};

void tinyjSBindingsToFlameSteelEngineGameToolkit_AddButton__private(CScriptVar *v, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto tinyJS = container->tinyJS;
    auto delegateLocked = spaceJaguarScriptController->delegate.lock();
    if (delegateLocked == nullptr) {
        throwRuntimeException("tinyjSBindingsToFlameSteelEngineGameToolkit_IsButtonPressed error: can't lock delegate");
    }
    auto title = v->getParameter("title")->getString();
    auto id = v->getParameter("id")->getString();
	delegateLocked->spaceJaguarScriptControllerDidRequestAddButtonWithId(spaceJaguarScriptController, title, id);
};

void tinyjSBindingsToFlameSteelEngineGameToolkit_destroyWindow__private(CScriptVar *, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto tinyJS = container->tinyJS;
    auto delegateLocked = spaceJaguarScriptController->delegate.lock();
    if (delegateLocked == nullptr) {
        throwRuntimeException("tinyjSBindingsToFlameSteelEngineGameToolkit_IsButtonPressed error: can't lock delegate");
    }
	delegateLocked->spaceJaguarScriptControllerDidRequestDestroyWindow(spaceJaguarScriptController);
};

string tinyjSBindingsToFlameSteelEngineGameToolkit_guiTextID() {
	tinyjSBindingsToFlameSteelEngineGameToolkit_guiTextID_num += 1;
	return "tinyJS_justText" + to_string(tinyjSBindingsToFlameSteelEngineGameToolkit_guiTextID_num);
};

void tinyjSBindingsToFlameSteelEngineGameToolkit_AddText__private(CScriptVar *v, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto tinyJS = container->tinyJS;
    auto delegateLocked = spaceJaguarScriptController->delegate.lock();
    if (delegateLocked == nullptr) {
        throwRuntimeException("tinyjSBindingsToFlameSteelEngineGameToolkit_IsButtonPressed error: can't lock delegate");
    }
    auto title = v->getParameter("text")->getString();
	title = "|" + title + "|";
    	auto id = tinyjSBindingsToFlameSteelEngineGameToolkit_guiTextID();
	delegateLocked->spaceJaguarScriptControllerDidRequestAddButtonWithId(spaceJaguarScriptController, title, id);
};

void tinyJSBindingsToFlameSteelEngineGameToolkit_Include(CScriptVar *v, void *pointerToTinyJS) {
    auto path = v->getParameter("text")->getString();
    cout << "Tiny-JS include: " << path << endl;
    auto includeScriptString = stringFromFileAtPath(path);
    CTinyJS *tinyJS = (CTinyJS*)pointerToTinyJS;
    tinyJS->execute(includeScriptString);
};

void tinyJSBindingsToFlameSteelEngineGameToolkit_enablePointerLock(CScriptVar *, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto tinyJS = container->tinyJS;
    auto delegateLocked = spaceJaguarScriptController->delegate.lock();
    if (delegateLocked == nullptr) {
        throwRuntimeException("tinyJSBindingsToFlameSteelEngineGameToolkit_enablePointerLock error: can't lock delegate");
    }
	delegateLocked->spaceJaguarScriptControllerDidRequestEnablePointerLock(spaceJaguarScriptController);
};

void tinyJSBindingsToFlameSteelEngineGameToolkit_disablePointerLock(CScriptVar *, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto tinyJS = container->tinyJS;
    auto delegateLocked = spaceJaguarScriptController->delegate.lock();
    if (delegateLocked == nullptr) {
        throwRuntimeException("tinyJSBindingsToFlameSteelEngineGameToolkit_enablePointerLock error: can't lock delegate");
    }
	delegateLocked->spaceJaguarScriptControllerDidRequestDisablePointerLock(spaceJaguarScriptController);
};

void tinyJSBindingsToFlameSteelEngineGameToolkit_pointerXdiff(CScriptVar *v, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto tinyJS = container->tinyJS;
    auto dataSourceLocked = spaceJaguarScriptController->dataSource.lock();
    if (dataSourceLocked == nullptr) {
        throwRuntimeException("tinyjSBindingsToFlameSteelEngineGameToolkit_IsKeyPressed error: can't lock dataSource");
    }
	auto pointerXdiff = dataSourceLocked->spaceJaguarScriptControllerAskingForPointerXdiff(spaceJaguarScriptController);
	v->getReturnVar()->setDouble(pointerXdiff);
};

void tinyJSBindingsToFlameSteelEngineGameToolkit_pointerYdiff(CScriptVar *v, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto tinyJS = container->tinyJS;
    auto dataSourceLocked = spaceJaguarScriptController->dataSource.lock();
    if (dataSourceLocked == nullptr) {
        throwRuntimeException("tinyjSBindingsToFlameSteelEngineGameToolkit_IsKeyPressed error: can't lock dataSource");
    }
	auto pointerYdiff = dataSourceLocked->spaceJaguarScriptControllerAskingForPointerYdiff(spaceJaguarScriptController);
	v->getReturnVar()->setDouble(pointerYdiff);
};

void tinyJSBindingsToFlameSteelEngineGameToolkit_drawTextIntoObjectRect(CScriptVar *v, void *context) {
	auto text = v->getParameter("text")->getString();

	auto objectName = v->getParameter("objectName")->getString();
	auto dX = v->getParameter("dX")->getInt();
	auto dY = v->getParameter("dY")->getInt();

    auto container = (EcmaScriptControllerCallContainer *) context;
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;

	TTF_Font *font = spaceJaguarScriptController->font;

	if (!font) {

    if(TTF_Init() != 0)
	{
       	 printf("TTF_Init: %s\n", TTF_GetError());
	        throw runtime_error("Can't load SDL2 TTF");
	    }

	    font = TTF_OpenFont("com.demensdeum.flamesteelengine.crystal.font.ttf", 16);
		spaceJaguarScriptController->font = font;

    	if(!font)
	    {
        	printf("TTF_OpenFont: %s\n", TTF_GetError());
	        throw runtime_error("TTF Can't load font");
	    }
	}

   auto dataSourceLocked = spaceJaguarScriptController->dataSource.lock();
    if (dataSourceLocked == nullptr) {
        throwRuntimeException("tinyJSBindingsToFlameSteelEngineGameToolkit_drawImageRectIntoObjectRect error: can't lock dataSource");
    }
    auto object = dataSourceLocked->spaceJaguarScriptControllerDidRequestObjectWithName(spaceJaguarScriptController, objectName);

    auto surfaceMaterial = FSEGTUtils::getObjectSurfaceMaterial(object);
    if (surfaceMaterial.get() == nullptr)
    {
        throwRuntimeException("Surface material is null... end");
    }

        auto surface = surfaceMaterial->material->surface;

	// per line text rendering
	std::istringstream iss(text);
	SDL_Color color = { 255, 255, 255, 255 };
        auto lineIndex = 0;
        for (string line; getline(iss, line); )
        {
            lineIndex+= 1;
            auto uiText = TTF_RenderText_Solid(font, line.c_str(), color);
            SDL_Rect destinationRect;
            destinationRect.x = dX;
            destinationRect.y = dY + lineIndex * 20;
            destinationRect.w = uiText->w;
            destinationRect.h = uiText->h;
            SDL_BlitSurface(uiText, nullptr, surface, &destinationRect);
            SDL_FreeSurface(uiText);
        }

};

void tinyJSBindingsToFlameSteelEngineGameToolkit_clearSurfaceOfObject(CScriptVar *v, void *context) {
	auto objectName = v->getParameter("objectName")->getString();

    auto container = (EcmaScriptControllerCallContainer *) context;
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;

   auto dataSourceLocked = spaceJaguarScriptController->dataSource.lock();
    if (dataSourceLocked == nullptr) {
        throwRuntimeException("tinyJSBindingsToFlameSteelEngineGameToolkit_drawImageRectIntoObjectRect error: can't lock dataSource");
    }
    auto object = dataSourceLocked->spaceJaguarScriptControllerDidRequestObjectWithName(spaceJaguarScriptController, objectName);

    auto surfaceMaterial = FSEGTUtils::getObjectSurfaceMaterial(object);
    if (surfaceMaterial.get() == nullptr)
    {
        throwRuntimeException("Surface material is null... end");
    }

        auto surface = surfaceMaterial->material->surface;
	SDL_FillRect(surface, NULL, SDL_MapRGBA(surface->format, 0, 0, 0, 0));
}

void tinyJSBindingsToFlameSteelEngineGameToolkit_connectToWebSockets(CScriptVar *v, void *context) {
	auto address = v->getParameter("address")->getString();

    auto container = (EcmaScriptControllerCallContainer *) context;
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;

	spaceJaguarScriptController ->webSocketsClient = make_shared<WebSocketsClient>(address);
	auto sharedController = spaceJaguarScriptController->shared_from_this();
	spaceJaguarScriptController ->webSocketsClient->connect(sharedController);
}

void tinyJSBindingsToFlameSteelEngineGameToolkit_sendDataToWebSockets(CScriptVar *v, void *context) {
    auto container = (EcmaScriptControllerCallContainer *) context;
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;

	if (spaceJaguarScriptController->webSocketsClient.get() == nullptr) {
		throwRuntimeException("Can't send data to disconnected server");
	}
	auto data = v->getParameter("data")->getString();
	spaceJaguarScriptController ->webSocketsClient->send(string(data));
}

void tinyJSBindingsToFlameSteelEngineGameToolkit_drawImageRectIntoObjectRect(CScriptVar *v, void *context) {
	auto imageName = v->getParameter("imageName")->getString();

	auto sX = v->getParameter("sX")->getInt();
	auto sY = v->getParameter("sY")->getInt();
	auto sW = v->getParameter("sW")->getInt();
	auto sH = v->getParameter("sH")->getInt();

	auto objectName = v->getParameter("objectName")->getString();
	auto dX = v->getParameter("dX")->getInt();
	auto dY = v->getParameter("dY")->getInt();
	auto dW = v->getParameter("dW")->getInt();
	auto dH = v->getParameter("dH")->getInt();

    auto container = (EcmaScriptControllerCallContainer *) context;
    shared_ptr<EcmaScriptController> spaceJaguarScriptController = container->spaceJaguarScriptController;
    auto tinyJS = container->tinyJS;
    auto dataSourceLocked = spaceJaguarScriptController->dataSource.lock();
    if (dataSourceLocked == nullptr) {
        throwRuntimeException("tinyJSBindingsToFlameSteelEngineGameToolkit_drawImageRectIntoObjectRect error: can't lock dataSource");
    }
    auto object = dataSourceLocked->spaceJaguarScriptControllerDidRequestObjectWithName(spaceJaguarScriptController, objectName);

    auto surfaceMaterial = FSEGTUtils::getObjectSurfaceMaterial(object);
    if (surfaceMaterial.get() == nullptr)
    {
        throwRuntimeException("Surface material is null... end");
    }

 	auto sourceRectangle = SDL_Rect();
        sourceRectangle.x = sX;
        sourceRectangle.y = sY;
        sourceRectangle.w = sW;
        sourceRectangle.h = sH;

	auto destinationRectangle = SDL_Rect();
	destinationRectangle.x = dX;
	destinationRectangle.y = dY;
	destinationRectangle.w = dW;
	destinationRectangle.h = dH;

	SDL_Surface *imageSurface = nullptr;

	map<string, void *> *keyToImageCache = &spaceJaguarScriptController->keyToImageCache;

	if (keyToImageCache->find(imageName) == keyToImageCache->end()) {
		cout << "IMG_LOAD" << endl;
		imageSurface = IMG_Load(imageName.c_str());
		(*keyToImageCache)[imageName] = imageSurface;
	}
	else {
		imageSurface = (SDL_Surface *) (*keyToImageCache)[imageName];
	}

	if (imageSurface == nullptr) {
		throwRuntimeException("Image surface material is null... end");
	}

        auto surface = surfaceMaterial->material->surface;
        SDL_BlitSurface(imageSurface, &sourceRectangle, surface, &destinationRectangle);

	surfaceMaterial->material->needsUpdate = true;
};

void EcmaScriptController::playMusic(string path) {
	music = make<Music>(path);
	music->play();
};

void EcmaScriptController::initialize() {
    callContainer->spaceJaguarScriptController = shared_from_this();
    callContainer->tinyJS = tinyJS;

    try {
        registerFunctions(tinyJS.get());
        registerMathFunctions(tinyJS.get());
        tinyJS->addNative("function setWindowTitle(text)", &tinyJSBindingsToFlameSteelEngineGameToolkit_setWindowTitle, callContainer.get());
        tinyJS->addNative("function include(text)", &tinyJSBindingsToFlameSteelEngineGameToolkit_Include, tinyJS.get());
        tinyJS->addNative("function print(text)", &tinyJSBindingsToFlameSteelEngineGameToolkit_Print, 0);
        tinyJS->addNative("function getObject__private(text)", &tinyjSBindingsToFlameSteelEngineGameToolkit_GetObject__private, callContainer.get());
        tinyJS->addNative("function addObject__private(name, modelPath, x, y, z, rX, rY, rZ, sX, sY, sZ, surfaceMaterialEnabled, surfaceWidth, surfaceHeight, layer)", &tinyjSBindingsToFlameSteelEngineGameToolkit_AddObject__private, callContainer.get());
        tinyJS->addNative("function updateObject__private(name, x, y, z, rX, rY, rZ, sX, sY, sZ)", &tinyjSBindingsToFlameSteelEngineGameToolkit_UpdateObject__private, callContainer.get());
        tinyJS->addNative("function removeObject(name)", &tinyjSBindingsToFlameSteelEngineGameToolkit_RemoveObject__private, callContainer.get());
        tinyJS->addNative("function playAnimation__private(objectName, animationName)", &tinyjSBindingsToFlameSteelEngineGameToolkit_PlayAnimation__private, callContainer.get());
        tinyJS->addNative("function prompt(text)", &tinyjSBindingsToFlameSteelEngineGameToolkit_Prompt, callContainer.get());
        tinyJS->addNative("function isKeyPressed(key)", &tinyjSBindingsToFlameSteelEngineGameToolkit_IsKeyPressed, callContainer.get());
	tinyJS->addNative("function playMusic(path)", &tinyJSBindingsToFlameSteelEngineGameToolkit_PlayMusic, callContainer.get());
        tinyJS->addNative("function exit(exitCode)", &tinyjSBindingsToFlameSteelEngineGameToolkit_exit, nullptr);
        tinyJS->addNative("function removeAllObjects__private()", &tinyjSBindingsToFlameSteelEngineGameToolkit_RemoveAllObjects__private, callContainer.get());
	 tinyJS->addNative("function addButton(title, id)", &tinyjSBindingsToFlameSteelEngineGameToolkit_AddButton__private, callContainer.get());
	tinyJS->addNative("function addText(text)",  &tinyjSBindingsToFlameSteelEngineGameToolkit_AddText__private, callContainer.get());
	 tinyJS->addNative("function isButtonPressed(id)", &tinyjSBindingsToFlameSteelEngineGameToolkit_IsButtonPressed__private, callContainer.get());
	tinyJS->addNative("function destroyWindow()", &tinyjSBindingsToFlameSteelEngineGameToolkit_destroyWindow__private, callContainer.get());
	tinyJS->addNative("function isPointerPressed()", &tinyjSBindingsToFlameSteelEngineGameToolkit_isPointerPressed__private, callContainer.get());
	tinyJS->addNative("function pressedPointerParameter(parameter)", &tinyjSBindingsToFlameSteelEngineGameToolkit_pressedPointerParameter__private, callContainer.get());
	tinyJS->addNative("function saveVariable(name)", &tinyJSBindingsToFlameSteelEngineGameToolkit_saveVariable, callContainer.get());
	tinyJS->addNative("function enablePointerLock()", &tinyJSBindingsToFlameSteelEngineGameToolkit_enablePointerLock, callContainer.get());
	tinyJS->addNative("function disablePointerLock()", &tinyJSBindingsToFlameSteelEngineGameToolkit_disablePointerLock, callContainer.get());
	tinyJS->addNative("function pointerXdiff()", &tinyJSBindingsToFlameSteelEngineGameToolkit_pointerXdiff, callContainer.get());
	tinyJS->addNative("function pointerYdiff()", &tinyJSBindingsToFlameSteelEngineGameToolkit_pointerYdiff, callContainer.get());
	tinyJS->addNative("function selectFile()", &tinyjSBindingsToFlameSteelEngineGameToolkit_selectFile, nullptr);
	tinyJS->addNative("function copyFileIntoCurrentPath(filePath)", &tinyjSBindingsToFlameSteelEngineGameToolkit_copyFileIntoCurrentPath, nullptr);
	tinyJS->addNative("function drawImageRectIntoObjectRect__private(imageName, sX, sY, sW, sH, objectName, dX, dY, dW, dH)",  &tinyJSBindingsToFlameSteelEngineGameToolkit_drawImageRectIntoObjectRect, callContainer.get());
	tinyJS->addNative("function drawTextIntoObjectRect__private(text, objectName, dX, dY)", &tinyJSBindingsToFlameSteelEngineGameToolkit_drawTextIntoObjectRect, callContainer.get());
	tinyJS->addNative("function clearSurfaceOfObject__private(objectName)", &tinyJSBindingsToFlameSteelEngineGameToolkit_clearSurfaceOfObject, callContainer.get());
	tinyJS->addNative("function ConnectToWebSockets(address)", &tinyJSBindingsToFlameSteelEngineGameToolkit_connectToWebSockets, callContainer.get());
	tinyJS->addNative("function SendDataToWebSockets(data)", &tinyJSBindingsToFlameSteelEngineGameToolkit_sendDataToWebSockets, callContainer.get());
    }
    catch (CScriptException *error) {
        cout << "Tiny-JS initialize error: " << error->text << endl;
    }
    isInitialized = true;
}

void EcmaScriptController::clientDidReceiveData(shared_ptr<Client> , string data) {
	cout << "rawData: " << data << endl;
	data = regex_replace(data, regex("\\\\"), "\\\\");
	data = regex_replace(data, regex("\""), "\\\"");
	cout << "data: " << data << endl;
	string executeString = "__global_webSocketsDataHandler.didReceiveWebSocketsData(\"";
	executeString += data;
	executeString += "\");";
	cout << "executeString: " << executeString << endl;

    try {
	tinyJS->execute(executeString);
    } catch (CScriptException *e) {
	throwRuntimeException(e->text);
	}
};

void EcmaScriptController::clearCache() {
	for (auto keyValue : keyToImageCache) {
		SDL_FreeSurface((SDL_Surface *)keyValue.second);
	}
	keyToImageCache.clear();
};

void EcmaScriptController::setScript(string script) {
    this->script = script;
}

void EcmaScriptController::step() {

	if (webSocketsClient.get() != nullptr) {
		webSocketsClient->step();
	}

    if (isInitialized == false) {
        initialize();
    }
    if (script.length() < 1) {
        return;
    }
    try {
        tinyJS->execute(script);
    }
    catch (CScriptException *error) {
        cout << "---[SCRIPT ERROR START]---" << endl;
        cout << "Tiny-JS error: " << error->text << endl;
        cout << "---[SCRIPT ERROR END]---" << endl;

        throwRuntimeException(error->text);
    }

};
