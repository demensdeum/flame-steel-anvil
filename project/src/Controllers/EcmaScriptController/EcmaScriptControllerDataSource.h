#ifndef SPACEJAGUARACTIONRPGLINUXSPACEJAGUARSCRIPTCONTROLLERDATASOURCE_H_
#define SPACEJAGUARACTIONRPGLINUXSPACEJAGUARSCRIPTCONTROLLERDATASOURCE_H_

#include <FlameSteelCore/Object.h>

using namespace FlameSteelCore;

namespace SpaceJaguarActionRPG {

class EcmaScriptController;

class EcmaScriptControllerDataSource {

public:
    virtual shared_ptr<Object> spaceJaguarScriptControllerDidRequestObjectWithName(shared_ptr<EcmaScriptController> spaceJaguarController, string  objectName) = 0;
    virtual bool spaceJaguarScriptControllerAskingIsKeyPressed(shared_ptr<EcmaScriptController> spaceJaguarController, string  key) = 0;
    virtual bool spaceJaguarScriptControllerAskingIsButtonPressed(shared_ptr<EcmaScriptController> spaceJaguarController, string  key) = 0;
    virtual bool spaceJaguarScriptControllerAskingIsPointerPressed(shared_ptr<EcmaScriptController> spaceJaguarController) = 0;
    virtual float spaceJaguarScriptControllerAskingForPressedPointerParameter(shared_ptr<EcmaScriptController> spaceJaguarController, string id) = 0;
    virtual int spaceJaguarScriptControllerAskingForPointerXdiff(shared_ptr<EcmaScriptController> spaceJaguarController) = 0;
    virtual int spaceJaguarScriptControllerAskingForPointerYdiff(shared_ptr<EcmaScriptController> spaceJaguarController) = 0;
};
};

#endif