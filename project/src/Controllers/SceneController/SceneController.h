#ifndef FLAMESTEELENGINEPROJECTSCENECONTROLLER_H_
#define FLAMESTEELENGINEPROJECTSCENECONTROLLER_H_

#include <Controllers/EcmaScriptController/EcmaScriptController.h>
#include <FlameSteelCore/SharedNotNullPointer.h>
#include <FlameSteelEngineGameToolkit/IO/Input/InputController.h>
#include <FlameSteelEngineGameToolkit/Controllers/GameController.h>
#include <Controllers/SceneController/SceneControllerDelegate.h>

using namespace Shortcuts;
using namespace SpaceJaguarActionRPG;

class SceneController: 
public GameController, 
public enable_shared_from_this<SceneController>, 
public EcmaScriptControllerDataSource, 
public EcmaScriptControllerDelegate {

public:
	SceneController(string script);

	weak_ptr<SceneControllerDelegate> delegate;

	void stopCleanAndNotify();
    void step();
    shared_ptr<Object> spaceJaguarScriptControllerDidRequestObjectWithName(shared_ptr<EcmaScriptController> spaceJaguarController, string  objectName);
    void spaceJaguarScriptControllerDidRequestAddObjectWithPath(shared_ptr<EcmaScriptController> spaceJaguarController, string name, string  modelPath, float x, float y, float z, float rX, float rY, float rZ, float sX, float sY, float sZ, bool surfaceMaterialEnabled, int surfaceWidth, int surfaceHeight, string layer);
    void spaceJaguarScriptControllerDidRequestUpdateObjectWithNameAndPositionXYZrXrYrZsXsYsZ(shared_ptr<EcmaScriptController>, string name, float x, float y, float z, float rX, float rY, float rZ, float sX, float sY, float sZ);
    void spaceJaguarScriptControllerDidRequestRemoveObjectWithName(shared_ptr<EcmaScriptController>, string name);
    bool spaceJaguarScriptControllerAskingIsKeyPressed(shared_ptr<EcmaScriptController> spaceJaguarController, string  key);
    void spaceJaguarScriptControllerDidRequestSetWindowTitle(shared_ptr<EcmaScriptController> spaceJaguarController, string title);
    void spaceJaguarScriptControllerDidRequestPlayAnimationForObjectWithName(shared_ptr<EcmaScriptController> spaceJaguarController, string animationName, string objectName);
    void spaceJaguarScriptControllerDidRequestRemoveAllObjects(shared_ptr<EcmaScriptController> spaceJaguarController);
	void spaceJaguarScriptControllerDidRequestAddButtonWithId(shared_ptr<EcmaScriptController> spaceJaguarController, string title, string id);
	void spaceJaguarScriptControllerDidRequestIsButtonPressedWithId(shared_ptr<EcmaScriptController> spaceJaguarController, string id);
	bool spaceJaguarScriptControllerAskingIsButtonPressed(shared_ptr<EcmaScriptController> spaceJaguarController, string  key);
	void spaceJaguarScriptControllerDidRequestDestroyWindow(shared_ptr<EcmaScriptController> spaceJaguarController);
	bool spaceJaguarScriptControllerAskingIsPointerPressed(shared_ptr<EcmaScriptController> spaceJaguarController);
	float spaceJaguarScriptControllerAskingForPressedPointerParameter(shared_ptr<EcmaScriptController> spaceJaguarController, string id);
	void spaceJaguarScriptControllerDidRequestEnablePointerLock(shared_ptr<EcmaScriptController> spaceJaguarController);
	void spaceJaguarScriptControllerDidRequestDisablePointerLock(shared_ptr<EcmaScriptController> spaceJaguarController);
	int spaceJaguarScriptControllerAskingForPointerXdiff(shared_ptr<EcmaScriptController> spaceJaguarController);
	int spaceJaguarScriptControllerAskingForPointerYdiff(shared_ptr<EcmaScriptController> spaceJaguarController);

	void cleanAndRestartScriptEngine();

    string script;

private:
	bool needToStopCleanAndNotify = false;

    NotNull<EcmaScriptController> scriptController;
    void initialize();
    bool isInitialized = false;
    NotNull<Object> camera;
    NotNull<InputController> inputController;
	
	vector<shared_ptr<Object> > guiObjects;

};

#endif
