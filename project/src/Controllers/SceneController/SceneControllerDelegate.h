#ifndef SPACEJAGUARACTIONRPGLINUXSPACEJAGUARSCENECONTROLLERDELEGATE_H_
#define SPACEJAGUARACTIONRPGLINUXSPACEJAGUARSCENECONTROLLERDELEGATE_H_

using namespace FlameSteelCore;

#include <memory>

using namespace std;

class SceneController;

class SceneControllerDelegate {

public:
    virtual void sceneControllerDidFinishCleaning(shared_ptr<SceneController> sceneController) = 0;
};

#endif