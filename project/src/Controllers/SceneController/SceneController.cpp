#include "SceneController.h"
#include <iostream>
#include <FlameSteelEngineGameToolkit/Utils/Factory.h>
#include <FlameSteelEngineGameToolkit/Utils/FSEGTUtils.h>
#include <FlameSteelCore/Utils.h>
#include <FlameSteelEngineGameToolkit/Data/Components/FSEGTFactory.h>
#include <FlameSteelEngineGameToolkitFSGL/Input/FSEGTIOFSGLInputController.h>
#include <FSGL/Data/Matrix/FSGLMatrix.h>
#include <FlameSteelEngineGameToolkit/Const/FSEGTConst.h>

using namespace std;
using namespace FlameSteelCore::Utils;

SceneController::SceneController(string script) {
    this->script = script;
}

void SceneController::stopCleanAndNotify() {
	needToStopCleanAndNotify = true;
}

void SceneController::initialize() {
    isInitialized = true;
    inputController = toNotNull(ioSystem->inputController);

    cleanAndRestartScriptEngine();
}

void SceneController::step() {

	if (needToStopCleanAndNotify) {
		objectsContext->removeAllObjects();
		    auto delegateLocked = delegate.lock();
		    if (delegateLocked != nullptr) {
			delegateLocked->sceneControllerDidFinishCleaning(shared_from_this());
			}
		return;
	}

    if (isInitialized == false) {
        initialize();
    }
    inputController->pollKey();
    scriptController->step();

    renderer->render(gameData);

    if (inputController->isExitKeyPressed() == true) {
        exit(10);
    }
}

void SceneController::cleanAndRestartScriptEngine() {
	needToStopCleanAndNotify = false;
    objectsContext->removeAllObjects();
    scriptController = make<EcmaScriptController>();
    scriptController->dataSource = shared_from_this();
    scriptController->delegate = shared_from_this();
    scriptController->setScript(script);
    scriptController->step();

    auto rawCamera = objectsContext->objectWithInstanceIdentifier(make_shared<string>("camera"));
	camera = NotNull<Object>(rawCamera, "Camera is null, add camera object to scene please, CRASHING!");

}

void SceneController::spaceJaguarScriptControllerDidRequestRemoveAllObjects(shared_ptr<EcmaScriptController> ) {
    objectsContext->removeAllObjects();
}

shared_ptr<Object> SceneController::spaceJaguarScriptControllerDidRequestObjectWithName(shared_ptr<EcmaScriptController>, string  objectName) {
    return objectsContext->objectWithInstanceIdentifier(make_shared<string>(objectName));
}

void SceneController::spaceJaguarScriptControllerDidRequestAddObjectWithPath(
	shared_ptr<EcmaScriptController>, 
	string name, 
	string  modelPath, 
	float x, 
	float y, 
	float z, 
	float rX, 
	float rY, 
	float rZ, 
	float sX, 
	float sY, 
	float sZ,
	bool surfaceMaterialEnabled,
	int surfaceWidth,
	int surfaceHeight,
	string layer
) {

    shared_ptr<string> modelPathSharedPtr;
    if (modelPath != "undefined") {
        modelPathSharedPtr = make_shared<string>(modelPath);
    }

    auto object = FSEGTFactory::makeOnSceneObject(
                      make_shared<string>(name),
                      make_shared<string>(name),
                      shared_ptr<string>(),
                      modelPathSharedPtr,
                      shared_ptr<string>(),
                      x, y, z,
                      sX, sY, sZ,
                      rX, rY, rZ
);

	if (surfaceMaterialEnabled) {
		auto surfaceMaterialComponent  = FSEGTFactory::makeSurfaceMaterialComponent(surfaceWidth, surfaceHeight);
		object->addComponent(surfaceMaterialComponent);
	}

	if (layer == "Scene") {
		// pass
	}
	else if (layer == "Screen") {
    		auto flag = FSEGTFactory::makeBooleanComponent();
    		flag->setInstanceIdentifier(make_shared<string>(FSEGTConstComponentsFlag2D));
		flag->setClassIdentifier(make_shared<string>(FSEGTConstComponentsFlag2D));
		object->addComponent(flag);
	}
	else if (layer == "Skybox") {
    		auto flag = FSEGTFactory::makeBooleanComponent();
    		flag->setInstanceIdentifier(make_shared<string>(FSEGTConstComponentsFlagSkybox));
		flag->setClassIdentifier(make_shared<string>(FSEGTConstComponentsFlagSkybox));
		object->addComponent(flag);
	}

    cout << "SceneController addObject with name: " << name << endl;


    objectsContext->addObject(object);
}

void SceneController::spaceJaguarScriptControllerDidRequestSetWindowTitle(shared_ptr<EcmaScriptController>, string title) {
    ioSystem->setWindowTitle(title);
};

void SceneController::spaceJaguarScriptControllerDidRequestEnablePointerLock(shared_ptr<EcmaScriptController> ) {
	auto fsglInputController = (FSEGTIOFSGLInputController *) inputController.sharedPointer().get();
	fsglInputController->isPointerLockEnabled = true;
};

void SceneController::spaceJaguarScriptControllerDidRequestDisablePointerLock(shared_ptr<EcmaScriptController> ) {
	auto fsglInputController = (FSEGTIOFSGLInputController *) inputController.sharedPointer().get();
	fsglInputController->isPointerLockEnabled = false;
};

int SceneController::spaceJaguarScriptControllerAskingForPointerXdiff(shared_ptr<EcmaScriptController> ) {
	return inputController->pointerXdiff;
};

int SceneController::spaceJaguarScriptControllerAskingForPointerYdiff(shared_ptr<EcmaScriptController> ) {
	return inputController->pointerYdiff;
};


void SceneController::spaceJaguarScriptControllerDidRequestUpdateObjectWithNameAndPositionXYZrXrYrZsXsYsZ(shared_ptr<EcmaScriptController>, string name, float x, float y, float z, float rX, float rY, float rZ, float sX, float sY, float sZ) {
    auto object = objectsContext->objectWithInstanceIdentifier(make_shared<string>(name));
    if (object.get() == nullptr) {
        auto errorString = string("SceneController update object error, no object with name: ");
        errorString += name;
        throwRuntimeException(errorString);
    }
    auto position = FSEGTUtils::getObjectPosition(object);
    position->x = x;
    position->y = y;
    position->z = z;

    auto rotation = FSEGTUtils::getObjectRotation(object);
    rotation->x = rX;
    rotation->y = rY;
    rotation->z = rZ;

	auto scale = FSEGTUtils::getObjectScale(object);
	scale->x = sX;
	scale->y = sY;
	scale->z = sZ;

    //cout << "spaceJaguarScriptControllerDidRequestUpdateObjectWithNameAndPositionXYZrXrYrZ: " << rZ << endl;

    objectsContext->updateObject(object);
};

void SceneController::spaceJaguarScriptControllerDidRequestRemoveObjectWithName(shared_ptr<EcmaScriptController>, string name) {
    auto object = objectsContext->objectWithInstanceIdentifier(make_shared<string>(name));
    if (object.get() == nullptr) {
        auto errorString = string("SceneController can't remove object, no object with name: ");
        errorString += name;
        throwRuntimeException(errorString);
    }
	objectsContext->removeObject(object);
};

bool SceneController::spaceJaguarScriptControllerAskingIsPointerPressed(shared_ptr<EcmaScriptController> ) {
	return inputController->isPointerPressed();
};

float SceneController::spaceJaguarScriptControllerAskingForPressedPointerParameter(shared_ptr<EcmaScriptController> , string id)  {
	return inputController->pressedPointerParameter(id);
}

bool SceneController::spaceJaguarScriptControllerAskingIsButtonPressed(shared_ptr<EcmaScriptController>, string  key) {
	return inputController->isButtonPressed(key);
}

bool SceneController::spaceJaguarScriptControllerAskingIsKeyPressed(shared_ptr<EcmaScriptController>, string  key) {
    if (key == "leftKey") {
        return inputController->isLeftKeyPressed();
    }
    else if (key == "rightKey") {
        return inputController->isRightKeyPressed();
    }
    else if (key == "upKey") {
        return inputController->isUpKeyPressed();
    }
    else if (key == "downKey") {
        return inputController->isDownKeyPressed();
    }
    else if (key == "jumpKey") {
        return inputController->isJumpKeyPressed();
    }
    return false;
}

void SceneController::spaceJaguarScriptControllerDidRequestAddButtonWithId(shared_ptr<EcmaScriptController> , string title, string id) {
    auto object = FSEGTFactory::makeOnScreenButton(title, id);
    objectsContext->addObject(object);
	guiObjects.push_back(object);
}

void SceneController::spaceJaguarScriptControllerDidRequestIsButtonPressedWithId(shared_ptr<EcmaScriptController> , string id) {
	inputController->isButtonPressed(id);
}

void SceneController::spaceJaguarScriptControllerDidRequestDestroyWindow(shared_ptr<EcmaScriptController> ) {
	for (auto object : guiObjects) {
		objectsContext->removeObject(object);
		printf("removed gui object\n");
	}
	guiObjects.clear();
}

void SceneController::spaceJaguarScriptControllerDidRequestPlayAnimationForObjectWithName(
    shared_ptr<EcmaScriptController>,
    string animationName,
    string objectName
)
{
    cout << "requested animation play: \"" << animationName << "\" for object with name: \"" << objectName << "\"" << endl;
    auto object = objectsContext->objectWithInstanceIdentifier(make_shared<string>(objectName));
    if (object.get() == nullptr) {
        auto errorString = string("SceneController play animation error, no object with name: ");
        errorString += objectName;
        throwRuntimeException(errorString);
    }

    FSEGTUtils::setCurrentAnimationNameForObject(animationName, object);
    objectsContext->updateObject(object);
};