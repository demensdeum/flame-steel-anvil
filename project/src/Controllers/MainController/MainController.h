#ifndef FLAMESTEELENGINEPROJECTMAINCONTROLLER_H_
#define FLAMESTEELENGINEPROJECTMAINCONTROLLER_H_

#include "State.h"
#include <FlameSteelEngineGameToolkitFSGL/IO/IOSystem.h>
#include <FlameSteelEngineGameToolkitFSGL/IO/Window/Window.h>
#include <FlameSteelEngineGameToolkit/Controllers/MainGameController.h>
#include <Controllers/SceneController/SceneController.h>
#include <FlameSteelCommonTraits/IOSystemParams.h>

using namespace FlameSteelProject::MainControllerState;

namespace FlameSteelEngineProject {

class MainController: public enable_shared_from_this<MainController>, public SceneControllerDelegate {

public:
    MainController(shared_ptr<string> startScriptPath = nullptr, NotNull<IOSystemParams> params = make<IOSystemParams>());

    void start();
    void switchToSceneController();
    void startGameLoop();
    void cleanAndRestart();

	void sceneControllerDidFinishCleaning(shared_ptr<SceneController> sceneController);

	virtual ~MainController() {};

private:
    shared_ptr<IOSystem> ioSystem;
    shared_ptr<Window> window;
    State state  =  started;
    shared_ptr<string> startScriptPath;
    NotNull<IOSystemParams> params;

    shared_ptr<FlameSteelEngine::GameToolkit::MainGameController> mainGameController;
    shared_ptr<SceneController> sceneController;

	void initializeSceneController();

};
}

extern "C" {
	void FlameSteelEngine_cleanAndRestartScriptEngine();
};
#endif

static FlameSteelEngineProject::MainController* FlameSteelEngineProject_GlobalMainController = nullptr;