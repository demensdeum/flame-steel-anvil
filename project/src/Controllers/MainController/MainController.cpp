#include "MainController.h"
#include <filesystem>
#include <iostream>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#include <emscripten/val.h>
#endif

using namespace FlameSteelEngineProject;

#ifdef SPACEJUARACTIONRPG_ANDROID_BUILD
#include "fschest.h"

void extractResourcesForAndroid() {
    char *archiveBuffer = nullptr;
    size_t size = 0;
    SDL_RWops *io = SDL_RWFromFile("files.fschest", "r");
    if (io != nullptr) {
        size = SDL_RWsize(io);
        archiveBuffer = (char *) malloc(size);
        SDL_RWread(io, archiveBuffer, sizeof(char), size);
    }
    io->close(io);

    chdir(SDL_AndroidGetInternalStoragePath());

    string outputPath = "files.fschest";
    auto fd = fopen(outputPath.c_str(), "wb");
    fwrite(archiveBuffer, sizeof(char), size, fd);
    fclose(fd);

    FSCHEST_extractChestToDirectory(outputPath.c_str(), SDL_AndroidGetInternalStoragePath());
};
#endif

MainController::MainController(shared_ptr<string> startScriptPath, NotNull<IOSystemParams> params) {
    this->startScriptPath = startScriptPath;
    this->params = params;
};

void MainController::sceneControllerDidFinishCleaning(shared_ptr<SceneController> ) {
	initializeSceneController();
};

void MainController::cleanAndRestart() {

	if (sceneController.get() != nullptr) {
		sceneController->stopCleanAndNotify();
		return;
	}
	else {
		initializeSceneController();
	}
};

void MainController::initializeSceneController() {
    string mainScript = "main.js";

    if (startScriptPath.get()) {
        mainScript = *startScriptPath.get();
    }

#ifndef __EMSCRIPTEN__
	if (filesystem::exists(mainScript) == false) {
		auto errorText = string("Can't find mainScript: ");
		errorText += mainScript;
		throwRuntimeException(errorText);
	}
	auto script = stringFromFileAtPath(mainScript);
#else
	string script;
	if (filesystem::exists(mainScript) == true) {
		script = stringFromFileAtPath(mainScript);
	}
	else {
		script = emscripten::val::global("mainScript").as<string>();
		cout << "main script string: " << script << endl;
	}
#endif

	if (sceneController.get() == nullptr) {
	    sceneController = make_shared<SceneController>(script);
	}
	else {
		sceneController->script  = script;
		sceneController->cleanAndRestartScriptEngine();
	}
	sceneController->delegate = shared_from_this();

    mainGameController->setControllerForState(sceneController, scene);
}

void MainController::start() {
    cout << "MainController::start()" << endl;

	FlameSteelEngineProject_GlobalMainController = this;

#ifdef SPACEJUARACTIONRPG_ANDROID_BUILD
    extractResourcesForAndroid();
#endif

    mainGameController = make_shared<FlameSteelEngine::GameToolkit::MainGameController>();

	cleanAndRestart();

    ioSystem = make_shared<IOSystem>();
    ioSystem->initialize(params);
    window = ioSystem->getWindow();

    mainGameController->setIOSystem(ioSystem->getFSGLIOSystem());
};

void MainController::switchToSceneController() {
    if (state == scene) {
        return;
    }
    state = scene;
    mainGameController->initializeGameFromState(scene);
}

void MainController::startGameLoop() {
    mainGameController->startGameLoop();
}

void FlameSteelEngine_cleanAndRestartScriptEngine() {
	if (FlameSteelEngineProject_GlobalMainController) {
		FlameSteelEngineProject_GlobalMainController->cleanAndRestart();
	}
}

