root=$(pwd)

cd project/include/FlameSteelFramework/flame-steel-engine/
git remote set-url origin git@gitlab.com:demensdeum/flame-steel-engine.git
cd $root

cd project/include/FlameSteelFramework/FlameSteelBattleHorn/
git remote set-url origin git@gitlab.com:demensdeum/FlameSteelBattleHorn.git
cd $root

cd project/include/FlameSteelFramework/FlameSteelCommonTraits/
git remote set-url origin git@gitlab.com:demensdeum/FlameSteelCommonTraits.git
cd $root

cd project/include/FlameSteelFramework/FlameSteelCore/
git remote set-url origin git@gitlab.com:demensdeum/FlameSteelCore.git
cd $root

cd project/include/FlameSteelFramework/FlameSteelEngineGameToolkit/
git remote set-url origin git@gitlab.com:demensdeum/FlameSteelEngineGameToolkit.git
cd $root

cd project/include/FlameSteelFramework/FlameSteelEngineGameToolkitFSGL/
git remote set-url origin git@gitlab.com:demensdeum/FlameSteelEngineGameToolkitFSGL.git
cd $root

cd project/include/FlameSteelFramework/FlameSteelSpiderToolkit/
git remote set-url origin git@gitlab.com:demensdeum/FlameSteelSpiderToolkit.git
cd $root

cd project/include/FlameSteelFramework/FSGL/
git remote set-url origin git@gitlab.com:demensdeum/FSGL.git
cd $root
