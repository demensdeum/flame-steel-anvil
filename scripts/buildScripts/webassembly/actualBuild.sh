#!/bin/bash
FLAME_STEEL_PROJECT_ROOT_DIRECTORY=${PWD}
source ~/Sources/3rdparty/emsdk/emsdk_env.sh
buildDirectory=build/webassembly
cd $buildDirectory
emcmake cmake -DRESOURCES_PATH=resources -DEMSCRIPTEN=1 -DFLAME_STEEL_PROJECT_ROOT_DIRECTORY=${FLAME_STEEL_PROJECT_ROOT_DIRECTORY} .
emmake make -j$(nproc)
