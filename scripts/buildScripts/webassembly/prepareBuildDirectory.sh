#!/bin/bash
buildDirectory=build/webassembly
mkdir -p $buildDirectory/FlameSteelEngineProject || true
mkdir -p $buildDirectory/resources || true
touch $buildDirectory/resources/emptyFileForPacker
cp project/include/FlameSteelFramework/flame-steel-engine/project/src/* $buildDirectory/resources/
cp project/include/FlameSteelFramework/flame-steel-engine/project/resources/* $buildDirectory/resources/
cp project/resources/textures/* $buildDirectory/resources/
cp project/resources/scripts/* $buildDirectory/resources/
cp project/resources/models/* $buildDirectory/resources/
cp project/resources/music/* $buildDirectory/resources/
cp project/resources/fonts/* $buildDirectory/resources/
cp scripts/buildScripts/webassembly/resources/index.html $buildDirectory/FlameSteelEngineProject/
cp scripts/utils/texturesScaler.py $buildDirectory
rm $buildDirectory/FlameSteelEngineProject/FlameSteelEngineProject.js || true
cp scripts/buildScripts/webassembly/resources/CMakeLists.txt $buildDirectory/CMakeLists.txt
